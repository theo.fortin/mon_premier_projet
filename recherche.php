<?php

/* 
 * controller qui va etre appellé par ajax pour faire une recherche

 */



//initialisation
include_once 'lib/init.php';
include_once 'classes/produits.php';

if(isset($_POST["texte"])) {
$texte = (string) trim(htmlentities($_POST["texte"]));
$sql = "SELECT * FROM `produits` WHERE `libelle` LIKE :texte ";
$param = [":texte" => "%$texte%"];
$req = BDDselect($sql, $param);

while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){

    ?>
    <div><?=$ligne['libelle']?></div>
<?php

}
}
//include 'templates/pages/test_rech.php';
<?php

/* 
 * controller qui permet d'afficher les tickets ouvert et en cours de traitemant
 */


//initialisation
include 'lib/init.php';
include 'classes/client.php';
include 'classes/vente.php';
include 'classes/tickets.php';

$sql = "SELECT * FROM `tickets` WHERE id_client =:id_client";
$param = [":id_client" => $_SESSION["id"]];
$req = BDDselect($sql, $param);
$result= [];

while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
   
$tickets = new tickets();
$tickets->set("problematique", $ligne["problematique"]);
$tickets->set("statut", $ligne["statut"]);
$tickets->set("id",$ligne["id"]);

$result[$ligne["id"]] = $tickets;
}
/*}*/
//affiche la page site_internet
include 'templates/pages/encours.php';

<?php

/*
 * Librairie pour les fonctions d'acces a la base donnée.
 */

function BDDopen() {
    //role: fonction qui permet d'acceder la la base de donnée
    //parametre: néant
    //retour: la bdd initialisé
    global $bdd;

    //si la base n'est pas initialisé on le fera
    if (!isset($bdd)) {
        $bdd = new PDO("mysql:host=localhost;dbname=projets_tickets_tfortin;charset=UTF8", "tfortin-exam", "17021995tF@");
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    }
    //retourner la base de donnée
    return $bdd;
}

function BDDselect($sql, $param) {
    //role: gere les requetes de selection dans la base de donnée
    //parametre:    - sql-> la requete préparée
    //              - param-> parametrer pour le tableau de valorisation des parametres
    //retour: le resultat de requete exécuté
    
    //on ouvre la base de donnée
    $bdd = BDDopen();
    
    //on prepare le resultat de la requete
    $req = $bdd->prepare($sql);

    // si le resultat de la requete est fausse a l'exécution des parametres -> message d'erreur
    if ($req->execute($param) === false) {
        echo "erreur BDDselect: parametre $param requete $req ";
        print_r($param);
        print_r($sql);
    }
     
    return $req;
    print_r($req);
}

function BDDquery($sql, $param) {
    //role: executer une requete quelquonque
    //parametre:    - sql-> la requete préparée
    //              - param-> parametrer pour le tableau de valorisation des parametres
    //retour: vrai
    
    //on ouvre la base de donnée
    $bdd = BDDopen();
    
    //on prepare le resultat de la requete
    $req = $bdd->prepare($sql);
    // si le resultat de la requete est fausse a l'exécution des parametres -> message d'erreur retourner faux

   if ($req->execute($param) === false) {
        echo "erreur BDDquery: parametre $param requete$req ";
        print_r($param);
        print_r($sql);
        return false;
    }
    

    return true;
}

function BDDlastid() {
    //role: récupere le dernier id inséré
    // parametre: $id ???????????????????????????????????????????????????????????????????
    //retour: la bdd qui accede au dernier id inséré
    
    //on ouvre la base de donnée
    $bdd = BDDopen();
    return $bdd->lastInsertId();
  
}

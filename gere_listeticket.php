<?php

/* 
 * controller qui permet d'afficher la liste des tickets suivi par le technicien

 */


//initialisation
include 'lib/init.php';
include 'classes/tickets.php';


$sql = "SELECT * FROM `tickets` WHERE id_technicien =:id_technicien";
$param = [":id_technicien" => $_SESSION["id"]];
$req = BDDselect($sql, $param);
$result= [];

while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
   
$tickets = new tickets();
$tickets->set("problematique", $ligne["problematique"]);
$tickets->set("libelle",$ligne["libelle"]);
$tickets->set("statut",$ligne["statut"]);
$tickets->set("id",$ligne["id"]);

$result[$ligne["id"]] = $tickets;
}
if(isset($_POST["resolution"])) {
$echange= new echange();
$echange->set("id_tickets", $_GET["id"]);
$echange->set("id_client", $_SESSION["id"]);
$echange->set("echange_client", $_POST["echange_client"]);
$echange->insert();
header("location: gere_resolution.php?id=$id");
}


//affiche la page resolution
include 'templates/pages/liste-ticketencours.php';
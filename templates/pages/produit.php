<!DOCTYPE html>
<!--
page qui permet l'affichage d'un produit unique
controller: gere_produit.php
-->
<html>
    <head>

        <meta charset="UTF-8">
        <title></title>
        <?php
        include "templates/fragments/header_client.php"
        ?>
    </head>
    <a href="../fragments/header_client.php"></a>
    <body>
        <h1><?= $produits->get("libelle") ?></h1>
        <p>reference: <?= $produits->get("ref") ?></p>
        <div style="display: flex; justify-content: space-around"><p><?= $produits->get("description") ?></p>
            <p style="font-size: 1.5rem; color: red"><?= $produits->get("pv") ?> €</p>
        </div>
        <?php
        if (isset($_SESSION["id"]) === TRUE) {
            echo '<form method="POST" name="panier">
            <input type="submit" name="panier" value="ajouter au panier">
            <input type="hidden" name="ref" value="'.$produits->get("ref").'">
            <input type="hidden" name="libelle" value="'.$produits->get("libelle").'">
        </form>
        <a href="gere_accueil.php">Retourner sur les autres produits</a>';
        } else {
            echo 'Vous devez vous connecter pour pouvoir ajouter un produit dans un panier';
        }
        ?>
    </body>
</html>

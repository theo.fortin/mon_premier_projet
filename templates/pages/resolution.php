<!DOCTYPE html>
<!--
page qui affiche les messages et le formulaire de reponse
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        include "templates/fragments/header_client.php"
        ?>
    </head>
    <body>
        <h3>Problématique: <?= $tickets->get("problematique") ?></h3>
        <p>statut de la demande: <?= $tickets->get("statut") ?></p>
        <fieldset>
            <?php
            foreach ($result as $echange) {

                echo "<div><p>" . $echange->get('echange_technicien') . "</p></div>"
                . "<div><p style='margin-left: 5%'>" . $echange->get('echange_client') . "</p></div>";
            }
            ?>
        </fieldset>
        <p>résolvez le probleme avec nos techniciens:</p>
        <form method="POST">
            <textarea name="echange_client"  rows="5" cols="33"></textarea>
            <input type="submit" name="resolution" value="envoyer">
        </form>
    </body>
</html>

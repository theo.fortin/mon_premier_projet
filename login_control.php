<?php

/*
 * controller qui permet la connexion d'un client
 */

//initialisation

include 'lib/init.php';
$bdd = new PDO("mysql:host=localhost;dbname=projets_tickets_tfortin;charset=UTF8", "tfortin-exam", "17021995tF@");
if (isset($_POST["connexion"])) {
    $mail = htmlentities($_POST["mail"]);
    $password = htmlentities($_POST["password"]);
    if (!empty($mail) AND !empty($password)) {
        $req = $bdd->prepare("SELECT * FROM `client` WHERE `mail` = ? AND `password` = ?");
        $t= $req->execute(array($mail, $password));
        $user = $req->rowCount();
        if ($user == 1) {
            $userinfo = $req->fetch();
            $_SESSION['id'] = $userinfo['id'];
            echo 'Vous etes connecté';
            header('location: gere_accueil.php');
        } else {
            echo 'mauvais mail ou mot de passe!';
        }
    } else {
        echo 'veuillez remplir tous les champs!';
    }
}

//on affiche la page
include 'templates/pages/form_connexion.php';

<?php

/*
 *Classe pour les fonctions en héritage
 */

class table {
    
    protected $pk = "id"; //clé primaire qui sera égale à l'id
    protected $table = ""; // la table ici est vide car leurs noms seront récupérer dans les tables étendu
    protected $champs = []; // idem que vue avant
    protected $valeurs = [];//










    public function get($nomchamp) {
        //role: récupérer les valeur d'un champs
        //parametre: $nomchamp
        //retour: retourne la valeur du champs celon le type de champs
        
        //si le nom des champs et l'objet champ n'apparait pas dans le tableau
        if (!in_array($nomchamp, $this->champs)) {
            
            //message d'erreur et rien retourner
            echo "erreur $nomchamp getter";
            return "";   
        }
        //si une valeur de nom champs est initialiser je la retourne
        if (isset($this->valeurs[$nomchamp])) {
            return $this->valeurs[$nomchamp];
            //sinon rien
        } else {
            return "";
        }
    }
    
    public function set($nomchamp, $val) {
        //role:initialiser et changer la valeur des champs
        //paramametre::    - $nomchamp
        //                 - $val
        
        //si le nom des champs et l'objet champ n'apparait pas dans le tableau
        if (!in_array($nomchamp, $this->champs)) {
            
            //message d'erreur et rien retourner
            echo "erreur setter";
            return false;   
        }
        //si une valeur de nom champs est initialiser je la retourne
        
            $this->valeurs[$nomchamp] = $val;
            return true;
           
        
    }
    
    public function setFromTab($tab) {
        //role: changer la valeur de plusieurs attributs
        //parametre: $tab
        // retour: vrai
        
        //créer une boucle
        foreach ($this->champs as $nomchamp) {
            if(isset($tab[$nomchamp]) or is_null($tab[$nomchamp])) {
                $this->set($nomchamp, $tab[$nomchamp]);
                
            }
        }
        return true;
    }
    
    public function loadById($id) {
        //role: récupérer les attributs d'un objet selon l'id dans la base de donnée
        //parametre: $id
        //retour: $ligne: resultat dans la bdd d'un tableau indexé
        
        //on ilitialise les parametres de la fonction pour les requetes de selection de bdd
        $sql = "SELECT * FROM ".$this->table." WHERE ".$this->pk." =:id";
        $param = [":id" => $id];
        
        $req = BDDselect($sql, $param);
        
        // récupérer les résultats d'un tableau indexé
        $ligne = $req->fetch(PDO::FETCH_ASSOC);
        
        //si $ligne n'est pas vide
        if(!empty($ligne)){
            $this->setFromTab($ligne);
            return $ligne;
        } else {
            //On supprime l'id pour éviter d'avoir une ancienne valeur
            $this->valeurs[$this->pk] = 0;  
            return false;
        }
    }
        public function loadByVente($id_vente) {
        //role: récupérer les attributs d'un objet selon l'id dans la base de donnée
        //parametre: $id
        //retour: $ligne: resultat dans la bdd d'un tableau indexé
        
        //on ilitialise les parametres de la fonction pour les requetes de selection de bdd
        $sql = "SELECT * FROM ".$this->table." WHERE id_vente =:id_vente";
        $param = [":id_vente" => $id_vente];
        
        $req = BDDselect($sql, $param);
        
        // récupérer les résultats d'un tableau indexé
        $ligne = $req->fetch(PDO::FETCH_ASSOC);
        
        //si $ligne n'est pas vide
        if(!empty($ligne)){
            $this->setFromTab($ligne);
            return $ligne;
        } else {
            //On supprime l'id pour éviter d'avoir une ancienne valeur
            $this->valeurs[$this->pk] = 0;  
            return false;
        }
    }
    
    public function insert() {
        //role: insérer un objet dans la base de donnée
        //parametre néant
        //retour: vrai si réussite faux sinon
        //on ilitialise les parametres de la fonction pour les requetes d'e selection'execution de bdd
        $sql = "INSERT INTO `".$this->table."` SET ";
        $set = [];
        $param = [];

        //faire une boucle
        foreach ($this->champs as $nomchamp) {
            if ($nomchamp !== "id") {
                $set[] = "`$nomchamp` =:$nomchamp";
                
                $param[":$nomchamp"] = $this->valeurs[$nomchamp];
            }
        }
        $sql .= implode(", ", $set);
        
        
        
        if (BDDquery($sql, $param)) {
            //pour mettre a jour l'id
            $this->valeurs[$this->pk] = BDDlastid();

            return true;
        } else {
            echo 'erreur insert';
            print_r($req);

            return false;
        }
    }

  public function update($id) {
        //role: modifie un objet dans la base de donnée
        //parametre néant
        //retour: fonction d'exécution bdd ou faux
        //on ilitialise les parametres de la fonction pour les requetes d'e selection'execution de bdd
        $sql = "UPDATE `".$this->table."` SET ";
        $set = [];
        $param = [];

        //faire une boucle
        foreach ($this->champs as $nomchamp) {
if ($nomchamp !== "id") {
                $set[] = "`$nomchamp` =:$nomchamp";
                
                $param[":$nomchamp"] = $this->valeurs[$nomchamp];

        }
        }
        $sql .= implode(", ", $set);
        
        $sql .= " WHERE `id` =$id";

        print_r($sql);
        
        if (BDDquery($sql, $param)!== -1) {
            $this->pk = 0;
print_r($req);
            return true;
        } else {
            echo 'erreur modif';
            print_r($req);

            return false;
        }
  }
  
  

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

<?php

/* 
 * controller qui permet f'afficher les informations d'un produit en particulier
 * on utilisera un $_get id pour récupérer son id est afficher ses informations
 */

//initialisation
include 'lib/init.php';
include 'classes/produits.php';
include 'classes/vente.php';
$produits = new produits();


$produits->loadById($_GET["id"]);
$libelle =$produits->get("libelle");
$ref = $produits->get("ref");
//ajouter une vente
if (isset($_POST["panier"])) {
$vente = new vente();
$vente->set("id_client", $_SESSION["id"]);
$vente->set("ref", $_POST["ref"]);
$vente->set("libelle",$_POST["libelle"]);
$vente->set("id_produit", $_GET["id"]);
$vente->set("date", date("d.m.y"));
$vente->insert();
header("location: gere_accueil.php");
}

//on affiche la page
include 'templates/pages/produit.php';


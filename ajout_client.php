 <?php
// controller pour l'ajout d'un client, il affichera une formulaire d'adhésion

include 'lib/init.php';
include 'classes/client.php';

if(isset($_POST["ajouter"])) {
    $client = new client();
$client->set("nom", $_POST["nom"]);
$client->set("prenom", $_POST["prenom"]);
$client->set("mail",$_POST["mail"]);
$client->set("password", $_POST["password"]);
$client->insert();
header("location: vendeur.php");
}

//page qui affiche le formulaire d'ajout client
include 'templates/pages/form_client.php';

 <?php
// controller de la page d'acceuil, elle affichera une liste des produits dans la page site_internet

include 'lib/init.php';
include 'classes/produits.php';


$bdd = new PDO("mysql:host=localhost;dbname=projets_tickets_tfortin;charset=UTF8", "tfortin-exam", "17021995tF@");
$req = $bdd->prepare("SELECT * FROM `produits`");
$req->execute();

$result= [];
while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
   
$prod = new produits();
$t =$prod->set("ref", $ligne["ref"]);
$prod->set("libelle", $ligne["libelle"]);
$prod->set("description",$ligne["description"]);
$prod->set("categorie",$ligne["categorie"]);
$prod->set("pv",$ligne["pv"]);
$prod->set("id",$ligne["id"]);
$result[$ligne["id"]] = $prod;
}
//affiche la page site_internet
include 'templates/pages/site_internet.php';

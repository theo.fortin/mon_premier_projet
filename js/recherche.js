/* 
 * permet d'appeler le controller de recherche et d'afficher les résultats avec ajax
 */

$(document).ready(function(){
    $('#texte').keyup(function(){
        $('#resultat').html('');
        
        var recherche = $(this).val();
        
        if (recherche !== ""){
            $.ajax({
                type: 'POST',
                url: 'recherche.php',
                data: {recherche: recherche},
                success: function(data){
                    if (data !== ""){
                        $('#resultat').append(data);
                        
                    } else {
                      document.getElementById('resultat').innerHTML = "<p>teste vide</p>";
                    }
                }
            });
        }
    });
});



<?php

/* 
 * Controller qui permet d'afficher la liste des clients
 */


include 'lib/init.php';
include 'classes/client.php';


$bdd = new PDO("mysql:host=localhost;dbname=projets_tickets_tfortin;charset=UTF8", "tfortin-exam", "17021995tF@");
$req = $bdd->prepare("SELECT * FROM `client`");
$req->execute();

$result= [];
while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
   
$client = new client();
$client->set("id",$ligne["id"]);
$client->set("nom", $ligne["nom"]);
$client->set("prenom", $ligne["prenom"]);
$client->set("mail",$ligne["mail"]);
$client->set("password",$ligne["password"]);

$result[$ligne["id"]] = $client;
}
//affiche la page site_internet
include 'templates/pages/liste_client.php';

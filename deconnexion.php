<?php

/* 
 * controller qui permet la déconnexion
 */

//Initialisation
include 'lib/init.php';

session_destroy();
header('location: gere_accueil.php');


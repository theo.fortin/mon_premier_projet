<?php

/* 
 * controller qui permet d'afficher la liste des tickets ouvert puis permettra de choisir le ticket sur lequel le technicien désire travailler

 */


//initialisation
include 'lib/init.php';
include 'classes/tickets.php';


$sql = "SELECT * FROM `tickets` WHERE statut =:statut";
$param = [":statut" => "ouvert"];
$req = BDDselect($sql, $param);
$result= [];

while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
   
$tickets = new tickets();
$tickets->set("problematique", $ligne["problematique"]);
$tickets->set("libelle",$ligne["libelle"]);
$tickets->set("statut",$ligne["statut"]);
$tickets->set("id",$ligne["id"]);

$result[$ligne["id"]] = $tickets;
}



//affiche la page resolution
include 'templates/pages/new_ticket.php';
<?php

/* 
 * controller qui permet les échanges pour la résolution d'un ticket
 * gere aussi le changement de statut du ticket

 */


//initialisation
include 'lib/init.php';
include 'classes/tickets.php';
include 'classes/echange.php';

$tickets= new tickets();
$tickets->loadById($_GET["id"]);

if(isset($_POST["resolution"])) {

$tickets->set("statut", "en cours");
$tickets->set("id_technicien", $_SESSION["id"]);
$tickets->update($_GET["id"]);
}

$tickets= new tickets();
$tickets->loadById($_GET["id"]);

$sql = "SELECT * FROM `echange` WHERE id_tickets =:id_tickets";
$param = [":id_tickets" => $_GET["id"]];
$req = BDDselect($sql, $param);
$result= [];

while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
   
$echange = new echange();
$echange->set("echange_client", $ligne["echange_client"]);
$echange->set("echange_technicien", $ligne["echange_technicien"]);
$echange->set("id",$ligne["id"]);

$result[$ligne["id"]] = $echange;
}


////////////////////////
/*$sql = "SELECT * FROM `echange` WHERE statut =:statut";
$param = [":statut" => "ouvert"];
$req = BDDselect($sql, $param);
$result= [];

while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
   
$tickets = new tickets();
$tickets->set("problematique", $ligne["problematique"]);
$tickets->set("libelle",$ligne["libelle"]);
$tickets->set("statut",$ligne["statut"]);
$tickets->set("id",$ligne["id"]);

$result[$ligne["id"]] = $tickets;
}

*/
$id= $_GET['id'];
if(isset($_POST["resolution"])) {
$echange= new echange();
$echange->set("id_tickets", $_GET["id"]);
$echange->set("id_technicien", $_SESSION["id"]);
$echange->set("echange_technicien", $_POST["echange_technicien"]);
$echange->insert();
header("location: gere_ticketencours.php?id=$id");
}
//affiche la page resolution
include 'templates/pages/ticket_encours.php';
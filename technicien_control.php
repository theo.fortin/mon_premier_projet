<?php

/*
 * controller qui permet la connexion d'un technicien
 */

//initialisation
include 'lib/init.php';

$bdd = new PDO("mysql:host=localhost;dbname=projets_tickets_tfortin;charset=UTF8", "tfortin-exam", "17021995tF@");
if (isset($_POST["connexion"])) {
    $pseudo = htmlentities($_POST["pseudo"]);
    $password = htmlentities($_POST["password"]);
    if (!empty($pseudo) AND !empty($password)) {
        $req = $bdd->prepare("SELECT * FROM `technicien` WHERE `pseudo` = ? AND `password` = ?");
        $t= $req->execute(array($pseudo, $password));
        $user = $req->rowCount();
        if ($user == 1) {
            $userinfo = $req->fetch();
            $_SESSION['id'] = $userinfo['id'];
            echo 'Vous etes connecté';
            header('location: technicien.php');
        } else {
            echo 'mauvais mail ou mot de passe!';
        }
    } else {
        echo 'veuillez remplir tous les champs!';
    }
}

//on affiche la page
include 'templates/pages/form_covendeur.php';

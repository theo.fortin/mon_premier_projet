<?php

/* 
 * controller qui permet de fermer un ticket

 */


//initialisation
include 'lib/init.php';
include 'classes/tickets.php';


$tickets= new tickets();
$tickets->loadById($_GET["id"]);

$tickets->set("statut", "résolu");
$tickets->update($_GET["id"]);
header("location: gere_listeticket.php");
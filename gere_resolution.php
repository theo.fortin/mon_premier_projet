<?php

/* 
 * controller qui permet de gérer l'avancement du tickets
 * il affichera une les détails du ticket et affichera les messages de résolution
 */


//initialisation
include 'lib/init.php';
include 'classes/client.php';
include 'classes/echange.php';
include 'classes/tickets.php';

$tickets = new tickets();
$tickets->loadById($_GET["id"]);
$id= $_GET['id'];
$sql = "SELECT * FROM `echange` WHERE id_tickets =:id_tickets";
$param = [":id_tickets" => $_GET["id"]];
$req = BDDselect($sql, $param);
$result= [];

while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
   
$echange = new echange();
$echange->set("echange_client", $ligne["echange_client"]);
$echange->set("echange_technicien", $ligne["echange_technicien"]);
$echange->set("id",$ligne["id"]);

$result[$ligne["id"]] = $echange;
}
if(isset($_POST["resolution"])) {
$echange= new echange();
$echange->set("id_tickets", $_GET["id"]);
$echange->set("id_client", $_SESSION["id"]);
$echange->set("echange_client", $_POST["echange_client"]);
$echange->insert();
header("location: gere_resolution.php?id=$id");
}


//affiche la page resolution
include 'templates/pages/resolution.php';
<?php

/*
 * controller qui permet de demander un tickets
 * il affichera le produit concerné et le formulaire de demande
 */


//initialisation
include 'lib/init.php';
include 'classes/client.php';
include 'classes/vente.php';
include 'classes/tickets.php';
include 'classes/produits.php';
$vente = new vente();
$vente->loadById($_GET["id"]);
$idproduit = $vente->get("id_produit");
$libelle = $vente->get("libelle");

if(isset( $_POST["ticket"])) {
    $tickets = new tickets();
    $tickets->set("id_client", $_SESSION["id"]);
    $tickets->set("id_produit", $idproduit);
    $tickets->set("libelle", $libelle);
    $tickets->set("problematique", $_POST["problematique"]);
    $tickets->set("statut", $_POST["statut"]);
    $tickets->set("id_vente", $_GET["id"]);
    $tickets->insert();
    header('location: gere_compte.php');
}
//affiche la page de formulaire de demande d'un ticket
include 'templates/pages/tickets.php';
